<?php
declare(strict_types=1);

namespace App\Importer;

use Illuminate\Support\Facades\Http;
use App\Repository\PlayerRepositoryInterface;

class PlayerImporter {

    private $player;
    /**
     * Create a new importer instance.
     *
     * @return void
     */
    public function __construct(PlayerRepositoryInterface $player)
    {
        $this->player = $player;
    }
    /**
     * 
     */
    public function getHttpPlayer()
    {
        $response = Http::get('https://fantasy.premierleague.com/api/bootstrap-static/');

        if ($response->successful()) {
            // Get the first 100 player
            return array_slice($response->json()['elements'], 0, 100);
        }
        return [];
    }
    public function getPlayer()
    {
        // Return the current player on the database
        return $this->player->showList();
    }
    public function importPlayer()
    {
            if (empty($this->getPlayer())) {
            // dd($this->getHttpPlayer());
            return $this->player->createPlayer($this->getHttpPlayer());
        }
    }
}