<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\{ EloquentRepositoryInterface,
    PlayerRepositoryInterface,
    UserRepositoryInterface
};
use App\Repository\Eloquent\{ BaseRepository,
    PlayerRepository,
    UserRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(PlayerRepositoryInterface::class, PlayerRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}