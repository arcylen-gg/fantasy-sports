<?php
declare(strict_types=1);

namespace App\Repository\Eloquent;

use App\Repository\PlayerRepositoryInterface;
use App\Repository\Eloquent\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Player;

class PlayerRepository extends BaseRepository implements PlayerRepositoryInterface
{
    /**
     * @param Player
     */

    public function __construct(Player $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Array
     */
    public function showList(): ?Array
    {
        return $this->model::all()->toArray();
    }

    /**
     * @return Model
     */
    public function singlePlayer(int $id): ?Model
    {
        $player = $this->model->find($id);
        $return = $this->model;
        $return->id = $player->id;
        $return->full_name = $player->first_name .' '.$player->second_name;
        return $return;
    }

    /**
     * @param array $attributes
     * 
     * @return Bool
     */
    public function createPlayer(array $attributes): Bool
    {
        foreach ($attributes as $playerData) {
            $player = new Player($playerData);
            $player->save();
        }
        return true;
    }
}