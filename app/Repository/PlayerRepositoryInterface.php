<?php
declare(strict_types=1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;
/**
 * Interface PlayerRepository
 * @package App\Repository;
 */
interface PlayerRepositoryInterface
{
    /**
     * @return array
    */
    public function showList(): ?Array;

    /**
     * @return model
    */
    public function singlePlayer(int $id): ?Model;

    /**
     * @param array $attributes
     * 
     * @return Bool
    */
    public function createPlayer(array $attributes): Bool;
}