<?php
declare(strict_types=1);

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepository
 * @package App\Repository;
 */
interface EloquentRepositoryInterface
{
    /**
     * @param array $attributes
    */
    public function create(array $attributes): Model;
}