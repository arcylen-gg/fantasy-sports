<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repository\PlayerRepositoryInterface;
use App\Importer\PlayerImporter;

class importPlayer extends Command
{
    private $player;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'player:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will import player';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PlayerRepositoryInterface $player)
    {
        parent::__construct();
        $this->player = $player;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $player = new PlayerImporter($this->player);
        $player->importPlayer();
        echo 'Player imported succesfully';
    }
}
