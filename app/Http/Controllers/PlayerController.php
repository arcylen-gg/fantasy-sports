<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Repository\PlayerRepositoryInterface;
use App\Importer\PlayerImporter;
use Illuminate\Support\Facades\Validator;

class PlayerController extends Controller
{
    private $player;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PlayerRepositoryInterface $player)
    {
        $this->player = $player;
    }

    //
    public function allPlayer()
    {
        return $this->player->showList();
    }
    //
    public function singlePlayer(int $id)
    {
        return $this->player->singlePlayer($id);
    }
}
